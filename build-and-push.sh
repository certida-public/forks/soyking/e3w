#!/bin/sh

set -eux

docker run --rm -w "/build" -v "$(pwd):/build" -it golang:1.17 /bin/sh -c 'go get gitlab.com/certida-public/forks/soyking/e3w/e3ch'

docker build --rm -t 'registry.gitlab.com/certida-public/forks/soyking/e3w:latest' .

pass show unlock || true

docker push registry.gitlab.com/certida-public/forks/soyking/e3w:latest
